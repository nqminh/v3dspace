/**
 *  v3DSpaceAppDelegate.h
 *  v3DSpace
 *
 *  Created by Sinbad Flyce on 11/1/13.
 *  Copyright VMODEV JSC 2013. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "CC3UIViewController.h"
#import "WelcomeViewController.h"

/*! Will be implemented soon.
 *  Thanks for looking it.
 */
@interface v3DSpaceAppDelegate : NSObject <UIApplicationDelegate,WelcomeViewDelegate>
{
	UIWindow* _window;
	CC3DeviceCameraOverlayUIViewController* _viewController;
    WelcomeViewController   *_welcomeController;
}
@end
