/**
 *  v3DSpaceAppDelegate.mm
 *  v3DSpace
 *
 *  Created by Sinbad Flyce on 11/1/13.
 *  Copyright VMODEV JSC 2013. All rights reserved.
 */

#import "v3DSpaceAppDelegate.h"
#import "vSpaceLayer.h"
#import "HouseScene.h"
#import "MixColliderScene.h"
#import "CC3CC2Extensions.h"

#define kAnimationFrameRate		60		// Animation frame rate
#define kHouseScene     100
#define kMixCollider    101

@implementation v3DSpaceAppDelegate

-(void) dealloc {
	[_window release];
	[_viewController release];
	[super dealloc];
}

#if CC3_CC2_1

/**
 * In cocos2d 1.x, the view controller and CCDirector are different objects.
 *
 * NOTE: As of iOS6, supported device orientations are an intersection of the mask established for the
 * UIViewController (as set in this method here), and the values specified in the project 'Info.plist'
 * file, under the 'Supported interface orientations' and 'Supported interface orientations (iPad)'
 * keys. Specifically, although the mask here is set to UIInterfaceOrientationMaskAll, to ensure that
 * all orienatations are enabled under iOS6, be sure that those settings in the 'Info.plist' file also
 * reflect all four orientation values. By default, the 'Info.plist' settings only enable the two
 * landscape orientations. These settings can also be set on the Summary page of your project.
 */
-(void) establishDirectorController
{
	
	// Establish the type of CCDirector to use.
	// Try to use CADisplayLink director and if it fails (SDK < 3.1) use the default director.
	// This must be the first thing we do and must be done before establishing view controller.
	if( ! [CCDirector setDirectorType: kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType: kCCDirectorTypeDefault];
	
	// Create the view controller for the 3D view.
	_viewController = [CC3DeviceCameraOverlayUIViewController new];
	_viewController.supportedInterfaceOrientations = UIInterfaceOrientationMaskAll;
	_viewController.viewShouldUseStencilBuffer = NO;		// Set to YES if using shadow volumes
	_viewController.viewPixelSamples = 1;					// Set to 4 for antialiasing multisampling
	
	// Create the CCDirector, set the frame rate, and attach the view.
	CCDirector *director = CCDirector.sharedDirector;
	director.runLoopCommon = YES;		// Improves display link integration with UIKit
	director.animationInterval = (1.0f / kAnimationFrameRate);
	director.displayFPS = YES;
	director.openGLView = _viewController.view;
	
	// Enables High Res mode on Retina Displays and maintains low res on all other devices
	// This must be done after the GL view is assigned to the director!
	[director enableRetinaDisplay: YES];
}
#endif

#if CC3_CC2_2

/*!
 * In cocos2d 2.x, the view controller and CCDirector are one and the same, and we create the
 * controller using the singleton mechanism. To establish the correct CCDirector/UIViewController
 * class, this MUST be performed before any other references to the CCDirector singleton!!
 *
 * NOTE: As of iOS6, supported device orientations are an intersection of the mask established for the
 * UIViewController (as set in this method here), and the values specified in the project 'Info.plist'
 * file, under the 'Supported interface orientations' and 'Supported interface orientations (iPad)'
 * keys. Specifically, although the mask here is set to UIInterfaceOrientationMaskAll, to ensure that
 * all orienatations are enabled under iOS6, be sure that those settings in the 'Info.plist' file also
 * reflect all four orientation values. By default, the 'Info.plist' settings only enable the two
 * landscape orientations. These settings can also be set on the Summary page of your project.
 */
-(void) establishDirectorController
{
	_viewController = CC3DeviceCameraOverlayUIViewController.sharedDirector;
	_viewController.supportedInterfaceOrientations = UIInterfaceOrientationMaskAll;
	_viewController.viewShouldUseStencilBuffer = NO;		// Set to YES if using shadow volumes
	_viewController.viewPixelSamples = 1;					// Set to 4 for antialiasing multisampling
	_viewController.animationInterval = (1.0f / kAnimationFrameRate);
	_viewController.displayStats = YES;
	[_viewController enableRetinaDisplay: YES];
}
#endif


/**
 *  establishWelcomController
 */
-(void) establishWelcomController
{
    _welcomeController = [[[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil] autorelease];
    _welcomeController.delegate = self;
}

/**
 *  attachLayerScene
 */
-(void)attachLayerScene:(NSNumber *)indexScene
{
    // Create the customized CC3Layer that supports 3D rendering.
    CC3Layer* cc3Layer = [vSpaceLayer layerWithController: _viewController];
    NSInteger idxScene = [indexScene integerValue];
    
    switch (idxScene) {
        case kHouseScene: {
            // House scene
            cc3Layer.cc3Scene = [HouseScene scene];
            break;
        }
        case kMixCollider: {
            // MixCollider scene
            cc3Layer.cc3Scene = [MixColliderScene scene];
            break;
        }            
        default:
            break;
    }	
	
	// Assign to a generic variable so we can uncomment options below to play with the capabilities
	CC3ControllableLayer* mainLayer = cc3Layer;
		
	// Set the layer in the controller
	_viewController.controlledNode = mainLayer;
	
	// Run the layer in the director
	CCScene *scene = [CCScene node];
	[scene addChild: mainLayer];
	[CCDirector.sharedDirector runWithScene: scene];
}

/**
 *  applicationDidFinishLaunching
 */
-(void) applicationDidFinishLaunching: (UIApplication*) application
{
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images.
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565. You can change anytime.
	CCTexture2D.defaultAlphaPixelFormat = kCCTexture2DPixelFormat_RGBA8888;
	
	// Establish the view controller and CCDirector (in cocos2d 2.x, these are one and the same)
	[self establishDirectorController];
	[self establishWelcomController];
    
	// Create the window, make the controller (and its view) the root of the window, and present the window
	_window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
	[_window addSubview: _welcomeController.view];
	_window.rootViewController = _welcomeController;
	[_window makeKeyAndVisible];

    //[self attachLayerScene];
}

/** Pause the cocos3d/cocos2d action. */
-(void) applicationWillResignActive: (UIApplication*) application
{
	[CCDirector.sharedDirector pause];
}

/** Resume the cocos3d/cocos2d action. */
-(void) resumeApp
{
    [CCDirector.sharedDirector resume];
}

-(void) applicationDidBecomeActive: (UIApplication*) application
{
	
	// Workaround to fix the issue of drop to 40fps on iOS4.X on app resume.
	// Adds short delay before resuming the app.
	[NSTimer scheduledTimerWithTimeInterval: 0.5f
									 target: self
								   selector: @selector(resumeApp)
								   userInfo: nil
									repeats: NO];
	
	// If dropping to 40fps is not an issue, remove above, and uncomment the following to avoid delay.
    // [self resumeApp];
}

/**
 *  applicationDidReceiveMemoryWarning
 */
-(void) applicationDidReceiveMemoryWarning: (UIApplication*) application
{
	[CCDirector.sharedDirector purgeCachedData];
}

/**
 *  applicationDidEnterBackground
 */
-(void) applicationDidEnterBackground: (UIApplication*) application
{
	[CCDirector.sharedDirector stopAnimation];
}

/**
 *  applicationWillEnterForeground
 */
-(void) applicationWillEnterForeground: (UIApplication*) application
{
	[CCDirector.sharedDirector startAnimation];
}

/**
 *  applicationWillTerminate
 */
-(void)applicationWillTerminate: (UIApplication*) application
{
	[CCDirector.sharedDirector.view removeFromSuperview];
	[CCDirector.sharedDirector end];
}

/**
 *  applicationSignificantTimeChange
 */
-(void) applicationSignificantTimeChange: (UIApplication*) application
{
	[CCDirector.sharedDirector setNextDeltaTimeZero: YES];
}

#pragma mark - WelcomeDelegate

/**
 *  didSelectScene
 */
-(void)welcomeView:(WelcomeViewController *)controller didSelectScene:(NSInteger)indexScene
{
    [_welcomeController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [_welcomeController presentModalViewController:_viewController animated:YES];
    [self performSelector:@selector(attachLayerScene:) withObject:[NSNumber numberWithInteger:indexScene] afterDelay:0.3];
}

@end
