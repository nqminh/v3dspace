/**
 *  vSpaceLayer.h
 *  v3DSpace
 *
 *  Created by Sinbad Flyce on 11/1/13.
 *  Copyright VMODEV JSC 2013. All rights reserved.
 */


#import "CC3Layer.h"
#import "Joystick.h"
#import "CCNodeAdornments.h"
#import "SettingViewController.h"

/*! A sample application-specific CC3Layer subclass.
 *  Will be implemented soon. Thanks for looking it.
 */
@interface vSpaceLayer : CC3Layer<SettingViewDelegate>
{
    @private
        Joystick* directionJoystick;
        Joystick* locationJoystick;
        AdornableMenuItemImage* settingMI;
}

@end
