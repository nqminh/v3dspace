//
//  vSpaceScene.mm
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "vSpaceScene.h"
#import "CC3BulletSolver.h"

@implementation CC3Scene(setting_extension)
- (void) updateSetting:(CC3Layer*)layer {}
@end

/**
 *  Abtract vSpaceScene class
 */
@implementation vSpaceScene

@synthesize playerDirectionControl = _playerDirectionControl;
@synthesize playerLocationControl = _playerLocationControl;

/**
 *  enableBulletSolver
 */
-(void) enableBulletSolver:(BOOL)enable
{
    if (enable) {
        if (_bulletSolver) [_bulletSolver release];
        _bulletSolver = [[CC3BulletSolver bulletSolver] retain];
        [_bulletSolver setGravity:0 y:-9.8 z:0];
    }
    else {
        if (_bulletSolver){
            [_bulletSolver release];
            _bulletSolver = nil;
        }
    }
}

@end
