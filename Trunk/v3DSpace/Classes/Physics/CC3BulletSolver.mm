//
//  CC3BulletSolver.m
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3BulletSolver.h"
#import "CC3Physics.h"
#import "CC3Node.h"
#import "cocos2d.h"

@implementation CC3BulletSolver

#pragma mark - PRIVATE

/**
 *  setup
 */
-(void) setup
{
    btBroadphaseInterface *broadphase;
    btDefaultCollisionConfiguration *collisionConfiguration;
    btSequentialImpulseConstraintSolver *solver;
    btCollisionDispatcher *dispatcher;

    broadphase = new btDbvtBroadphase();
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    solver = new btSequentialImpulseConstraintSolver();
    _dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConfiguration);
    _physicsObjects = [NSMutableArray new];
}

#pragma mark - PUBLIC

/**
 *  bulletSolver
 */
+(id) bulletSolver
{
    return [[[self alloc] init] autorelease];
}

/**
 *  init
 */
-(id) init
{
    self = [super init];
    
    if (self != nil) {
        [self setup];
    }    
    return self;
}

/**
 *  dealloc
 */
-(void) dealloc
{
    [_lastStepTime release];
    [_physicsObjects release];
    if (_dynamicsWorld != NULL) delete _dynamicsWorld;
    
    [super dealloc];
}

/**
 *  getDynamicsWorld
 */
-(btDiscreteDynamicsWorld*) getDynamicsWorld
{
    return _dynamicsWorld;
}

/**
 *  addPhysics
 */
-(void) addPhysics:(CC3Physics *)phyObj
{
    if (_dynamicsWorld != NULL) {
        
        _dynamicsWorld->addRigidBody((btRigidBody*)phyObj.collisionObject);
        [_physicsObjects addObject:phyObj];
    }
}

/**
 *  removePhysics
 */
-(void) removePhysics:(CC3Physics*)phyObj
{
    if (_dynamicsWorld != NULL) {
        
        _dynamicsWorld->removeRigidBody((btRigidBody*)phyObj.collisionObject);
        [_physicsObjects removeObject:phyObj];
    }
}

/**
 *  removePhysicsAll
 */
-(void)removePhysicsAll
{
    if (_dynamicsWorld != NULL) {
        for (CC3Physics *phyObj in _physicsObjects) {
             _dynamicsWorld->removeRigidBody((btRigidBody*)phyObj.collisionObject);
        }
        [_physicsObjects removeAllObjects];
    }
}

/**
 *  setGravity
 */
- (void) setGravity:(float)x y:(float)y z:(float)z
{
	_dynamicsWorld->setGravity(btVector3(x, y, z));
}

/**
 *  syncTranformation
 */
-(void) syncTranformation
{
    _dynamicsWorld->stepSimulation(1.f/60.f,10);
    
    // Update tranformation
    for (CC3Physics *object in _physicsObjects) {
        btTransform gTrans;
        btRigidBody *rigidBody = (btRigidBody*)object.collisionObject;
        rigidBody->getMotionState()->getWorldTransform(gTrans);
        btVector3 gPos = gTrans.getOrigin();
        btVector3 axis = gTrans.getRotation().getAxis();
        float angle = gTrans.getRotation().getAngle();
        CC3Vector4 quaternion;
        float sinAngle;
        angle *= 0.5f;
        axis = axis.normalized();
        sinAngle = sin(angle);
        quaternion.x = (axis.getX() * sinAngle);
        quaternion.y = (axis.getY() * sinAngle);
        quaternion.z = (axis.getZ() * sinAngle);
        quaternion.w = cos(angle);
        object.node.location = CC3VectorMake(gPos.getX(), gPos.getY(), gPos.getZ());
        object.node.quaternion = quaternion;
    }
    
    // Occur the collision between objects
    int nManifolds = _dynamicsWorld->getDispatcher()->getNumManifolds();
    for(int i = 0; i < nManifolds; i++) {
        btPersistentManifold* contactManifold = _dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
        btCollisionObject* obA = static_cast<btCollisionObject*>(contactManifold->getBody0());
        btCollisionObject* obB = static_cast<btCollisionObject*>(contactManifold->getBody1());        
        
        int nContacts = contactManifold->getNumContacts();
        for (int j = 0;j < nContacts; j++) {
            btManifoldPoint& pt = contactManifold->getContactPoint(j);
            btVector3 ptA = pt.getPositionWorldOnA();
            btVector3 ptB = pt.getPositionWorldOnB();            
        }
    }
}

@end
