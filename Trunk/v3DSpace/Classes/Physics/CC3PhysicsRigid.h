//
//  CC3PhysicsRigid.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3Physics.h"
#import "CC3Node.h"

class btRigidBody;

@interface CC3PhysicsRigid : CC3Physics
{    
    @private
        btRigidBody * _rigidBody;
}

/**
 *  initWithNode:name:andRigidBody
 */
-(id) initWithNode:(CC3Node *)node name:(NSString *)name andRigidBody:(btRigidBody *)rigidBody;

/**
 *  applyForce:withPosition
 */
-(void) applyForce:(CC3Vector)force withPosition:(CC3Vector)position;

/**
 *  applyImpulse:withPosition
 */
-(void) applyImpulse:(CC3Vector)force withPosition:(CC3Vector)position;


@end
