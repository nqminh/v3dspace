//
//  PhysicsNode.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CC3MeshNode.h"
#import "CC3Node.h"

@class CC3PhysicsWorld;
@class CC3Physics;

/**
 *  CollisionShape
 */
enum {
    CollisionShapeUnknown   = 0,
    CollisionShapeMesh      = 1,
    CollisionShapeBox       = 2,
    CollisionShapeSphere    = 3,
    CollisionShapePlane     = 4
};

typedef NSInteger CollisionShape;

/**
 *  Constance to detect type 3d object from its name
 */
NSString * const kGroundPrefix  = @"G__";
NSString * const kBoxPrefix     = @"B__";
NSString * const kSpherePrefix  = @"S__";


/**
 *  Physics extentions
 */
@interface CC3Node (Physics)

@property (nonatomic,readonly,getter = getPhysics) CC3Physics *physics;

/**
 *  addPhysicsActiveRigidBody:mass:restitution
 */
-(BOOL) addPhysicsActiveRigidBody:(CollisionShape)typeShape mass:(float)mass restitution:(float)restitution;

/**
 *  addPhysicsPassiveRigidBody:restitution
 */
-(BOOL) addPhysicsPassiveRigidBody:(CollisionShape)typeShape restitution:(float)restitution;

@end

