//
//  CC3Physics.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import <Foundation/Foundation.h>

class btCollisionObject;
class btPoint2PointConstraint;
class btCollisionShape;
@class CC3Node;

@interface CC3Physics : NSObject {
    
@protected
    NSString * _name;
    CC3Node  * _node;
    btPoint2PointConstraint * _constraint2p;
    btCollisionShape * _shape;
}

/**
 *  name
 */
@property (nonatomic,retain) NSString *name;

/**
 *  @getNode
 */
@property (nonatomic,readonly,getter = getNode) CC3Node *node;

/**
 *  @collisionObject
 *  It's tbRigidBody or btSoftBody or btGhostObject
 */
@property (nonatomic,readonly,getter = getCollisionObject) btCollisionObject *collisionObject;

/**
 *  constraint
 */
@property (nonatomic,readonly,getter = getConstraint2p) btPoint2PointConstraint *_constraint2p;

/**
 *  @shape
 *  It's btBoxShape or btSphereShape or btConvexHullShape, ...
 */
@property (nonatomic,readonly,getter = getShape) btCollisionShape *shape;

/**
 *  initWithNode:name
 */
-(id) initWithNode:(CC3Node*)node name:(NSString *)name;

@end
