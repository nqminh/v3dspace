//
//  CC3PhysicsRigidActive.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3PhysicsRigid.h"

class btRigidBody;

/**
 *  CC3PhysicsRigidActive class
 */
@interface CC3PhysicsRigidActive : CC3PhysicsRigid
@end
