//
//  CC3PhysicsRigid.m
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3PhysicsRigid.h"
#import "btBulletDynamicsCommon.h"

@implementation CC3PhysicsRigid

/**
 *  initWithNode:name:adbRigidBody
 */
-(id) initWithNode:(CC3Node *)node name:(NSString *)name andRigidBody:(btRigidBody *)rigidBody
{
    self = [super initWithNode:node name:name];
    
    if (self != nil) {
        _rigidBody = rigidBody;
        _shape = _rigidBody->getCollisionShape();
    }
    return self;
}

/**
 *  getCollisionObject
 */
-(btCollisionObject *) getCollisionObject
{
    return (btCollisionObject *)_rigidBody;
}

/**
 *  applyForce:withPosition
 */
-(void) applyForce:(CC3Vector)force withPosition:(CC3Vector)position
{
	btVector3 bodyForce(force.x, force.y, force.z);
	btVector3 bodyPosition(position.x, position.y, position.z);
	_rigidBody->applyForce(bodyForce, bodyPosition);
}

/**
 *  applyImpulse:withPosition
 */
-(void) applyImpulse:(CC3Vector)force withPosition:(CC3Vector)position
{
    btVector3 bodyForce(force.x, force.y, force.z);
	btVector3 bodyPosition(position.x, position.y, position.z);
	_rigidBody->applyImpulse(bodyForce, bodyPosition);
}

@end
