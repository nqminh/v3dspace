//
//  CC3Physics.m
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3Physics.h"
#import "btBulletDynamicsCommon.h"
#import "CC3Node.h"

@implementation CC3Physics

@synthesize name = _name;

-(id) init
{
    self = [super init];
    
    if (self != nil) {
        _name = @"";
    }    
    return self;
}

/**
 *  getCollisionObject
 */
-(btCollisionObject *) getCollisionObject
{
    return NULL;
}

/**
 *  getConstraint
 */
-(btPoint2PointConstraint *) getConstraint2p
{
    return _constraint2p;
}

/**
 *  getShape
 */
-(btCollisionShape *) getShape
{
    return _shape;
}

/**
 *  getNode
 */
-(CC3Node *) getNode
{
    return _node;
}


/**
 *  initWithNode:name
 */
-(id) initWithNode:(CC3Node*)node name:(NSString *)name
{
    self = [super init];
    
    if (self != nil) {
        _name = [name retain];
        _node = [node retain];
    }
    return self;
}

@end
