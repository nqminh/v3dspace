//
//  PhysicsNode.mm
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

extern "C" {
#import "CC3Foundation.h"
#import <objc/runtime.h>    
};

#import "CC3Node+Physics.h"
#import "CC3PODResourceNode.h"
#import "btBulletDynamicsCommon.h"
#import "CC3MeshNode.h"
#import "CC3PODMeshNode.h"
#import "CC3VertexArrayMesh.h"
#import "CC3Physics.h"
#import "CC3PhysicsRigid.h"
#import "CC3PhysicsRigidActive.h"
#import "CC3PhysicsRigidPassive.h"

static const char *cPhysicsObjectTag = "cPhysicsObjectTag";
static const char *cMassTag = "cMassTag";
static const char *cRestitutionTag = "cRestitutionTag";

#define VERTEX_SIZE 8

/**
 *  CC3Node PhysicEx class
 *  Now, It'll have working better
 */
@implementation CC3Node (Physics)

@dynamic physics;

#pragma mark - PRIVATE

/**
 *  setPhysicsObject
 */
-(void) setPhysics:(CC3Physics*)physicsObj
{
    CC3Physics *prevObj = [self getPhysics];
    if (prevObj != nil) [prevObj release];
    objc_setAssociatedObject(self, cPhysicsObjectTag, physicsObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 *  createCollisionShapeWithStaticMesh
 */
-(btCollisionShape*) createCollisionShapeWithPassiveMesh:(BOOL)isPassive
{
    CC3MeshNode *nodeMesh = (CC3MeshNode*)self;
    btCollisionShape *shape = NULL;
    
    float* vertexData = (float*)(nodeMesh.mesh.vertexLocations.vertices);
    GLushort* gIndices = (GLushort*)(nodeMesh.mesh).vertexIndices.vertices;
    int vertexCount = nodeMesh.mesh.vertexLocations.vertexCount;
    int gIndiceCount = (nodeMesh.mesh).vertexIndices.vertexCount;    
    
    CC3Mesh* mesh = nodeMesh.mesh;
    btTriangleIndexVertexArray* tiva = new btTriangleIndexVertexArray();
    btIndexedMesh im;
    im.m_vertexBase = (unsigned char*)mesh.vertexLocations.vertices;
    im.m_vertexStride = mesh.vertexLocations.vertexStride;
    im.m_numVertices = mesh.vertexLocations.vertexCount;
    im.m_triangleIndexBase = (const unsigned char*)mesh.vertexIndices.vertices;
    im.m_triangleIndexStride = mesh.vertexIndices.vertexStride * 3;
    im.m_numTriangles = mesh.vertexIndices.vertexCount / 3;
    im.m_indexType = PHY_SHORT;
    im.m_vertexType = PHY_FLOAT;
    tiva->addIndexedMesh(im,PHY_SHORT);
    [mesh retainVertexIndices];
    [mesh retainVertexLocations];
    
    if (isPassive)
        shape = new btBvhTriangleMeshShape(tiva,true);
    else
        shape = new btConvexTriangleMeshShape(tiva);
    
    return shape;
}

/**
 *  createCollisionShapeWithPassive:andTypeShape
 */
-(btCollisionShape*) createCollisionShapeWithPassive:(BOOL)isPassive andTypeShape:(CollisionShape)typeShape
{
    CC3Vector pp = self.location;
    btVector3 boxCenter = btVector3(pp.x, pp.y, pp.z);
    CC3Vector vBox = CC3BoxSize(self.boundingBox);
    //vBox = CC3VectorScale(vBox, self.scale);
    btVector3 haflBoxSize = btVector3(vBox.x,vBox.y,vBox.z) * 0.5;
	btCollisionShape *shape = NULL;
    
    if (typeShape == CollisionShapeMesh) {
        shape = [self createCollisionShapeWithPassiveMesh:isPassive];
    }
    else {
        switch (typeShape) {
            case CollisionShapeBox: {
                shape = new btBoxShape(haflBoxSize);
                break;
            }
            case CollisionShapeSphere: {
                shape = new btSphereShape(haflBoxSize.x());
                break;
            }
            case CollisionShapePlane: {
                break;
            }
            default:
                break;
        }
    }
    
   shape->setLocalScaling(btVector3(self.scale.x,self.scale.y,self.scale.z));
    
    return shape;
}

#pragma mark - PUBLIC

/**
 *  getPhysicsObject
 */
-(CC3Physics*) getPhysics
{
    return objc_getAssociatedObject(self,cPhysicsObjectTag);
}

/**
 *  addPhysicsActiveRigidBody:mass:restitution
 */
-(BOOL) addPhysicsActiveRigidBody:(CollisionShape)typeShape mass:(float)mass restitution:(float)restitution
{
    if (self.physics != nil) {
        return NO;
    }
    
    CC3Quaternion qn = self.quaternion;
    btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(btQuaternion(qn.x,qn.y,qn.z,qn.w), btVector3(self.location.x, self.location.y, self.location.z)));
	btCollisionShape *shape = [self createCollisionShapeWithPassive:NO andTypeShape:typeShape];
    btVector3 localInertia(0, 0, 0);
    
    if (typeShape != CollisionShapeMesh)
        shape->calculateLocalInertia(mass, localInertia);
    
    btRigidBody * rigidBody = new btRigidBody(mass, motionState, shape, localInertia);
	rigidBody->setRestitution(restitution);
	rigidBody->setActivationState(DISABLE_DEACTIVATION);    
    
    if (mass > 0) {        
        rigidBody->setDamping(0.0, 0.0);
        rigidBody->setFriction(1);
    }
    
    CC3Physics *phyObj = [[[CC3PhysicsRigidActive alloc] initWithNode:self name:self.name andRigidBody:rigidBody] autorelease];
    [self setPhysics:phyObj];
    
    return YES;
}

/**
 *  addPhysicsPassiveRigidBody:restitution
 */
-(BOOL) addPhysicsPassiveRigidBody:(CollisionShape)typeShape restitution:(float)restitution
{
    if (self.physics != nil) {
        return NO;
    }
    
    CC3Quaternion qn = self.quaternion;
    btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(btQuaternion(qn.x,qn.y,qn.z,qn.w), btVector3(self.location.x, self.location.y, self.location.z)));
	btCollisionShape *shape = [self createCollisionShapeWithPassive:YES andTypeShape:typeShape];
    btVector3 localInertia(0, 0, 0);
    
    if (typeShape != CollisionShapeMesh)
        shape->calculateLocalInertia(0.0f, localInertia);
    
    btRigidBody * rigidBody = new btRigidBody(0.0f, motionState, shape, localInertia);
	rigidBody->setRestitution(restitution);
	rigidBody->setActivationState(DISABLE_DEACTIVATION);
        
    CC3Physics *phyObj = [[[CC3PhysicsRigidPassive alloc] initWithNode:self name:self.name andRigidBody:rigidBody] autorelease];
    [self setPhysics:phyObj];
    
    return YES;   
}

@end