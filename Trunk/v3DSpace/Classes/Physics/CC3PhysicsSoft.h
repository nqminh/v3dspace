//
//  CC3PhysicsSoft.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3Physics.h"

class btSoftBody;

@interface CC3PhysicsSoft : CC3Physics {
    
@private
    btSoftBody * _softBody;
}

@end
