//
//  CC3BulletSolver.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "btBulletDynamicsCommon.h"

@class CC3Physics;
class btRigidBody;
class btDiscreteDynamicsWorld;
class btCollisionShape;

/**
 *  CC3BulletSolver class - a Physics world
 */
@interface CC3BulletSolver : NSObject {
    
@private
    btDiscreteDynamicsWorld * _dynamicsWorld;
    NSDate * _lastStepTime;
    NSMutableArray * _physicsObjects;
    btCollisionObject * _objects;
}

/**
 *  getDynamicsWorld
 */
@property (nonatomic,readonly,getter = getDynamicsWorld) btDiscreteDynamicsWorld *dynamicsWorld;

/**
 *  bulletSolver
 */
+(id) bulletSolver;

/**
 *  addPhysics
 */
-(void) addPhysics:(CC3Physics *)phyObj;

/**
 *  removePhysics
 */
-(void) removePhysics:(CC3Physics*)phyObj;

/**
 *  removePhysicsAll
 */
-(void) removePhysicsAll;

/**
 *  setGravity
 */
- (void) setGravity:(float)x y:(float)y z:(float)z;

/**
 *  syncTranformation
 */
-(void) syncTranformation;

@end
