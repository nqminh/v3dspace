//
//  CC3PhysicsPassiveRigid.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/27/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3Physics.h"
#import "CC3PhysicsRigid.h"

@interface CC3PhysicsRigidPassive : CC3PhysicsRigid

@end
