//
//  vSpaceScene.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3Scene.h"

@class CC3PhysicsWorld;
@class CC3BulletSolver;

@interface CC3Scene (setting_extension)
- (void) updateSetting:(CC3Layer*)layer;
@end

/**
 *  Abtract vSpaceScene class
 */
@interface vSpaceScene : CC3Scene {
    
@protected
    CGPoint _playerDirectionControl;
    CGPoint _playerLocationControl;
    CC3PhysicsWorld *_physicsWorld;
    CC3BulletSolver *_bulletSolver;
}

/**
 *  The solver of collision world
 */
@property(nonatomic, readonly) CC3BulletSolver* bulletSolver;


/**
 * This property controls the velocity of the change in direction of the 3D camera
 * (a proxy for the player). This property is set by the CC3Layer, from the velocity
 * of the corresponding joystick control.
 *
 * The initial value of this property is CGPointZero.
 */
@property(nonatomic, assign) CGPoint playerDirectionControl;

/**
 * This property controls the velocity of the change in location of the 3D camera
 * (a proxy for the player). This property is set by the CC3Layer, from the velocity
 * of the corresponding joystick control.
 *
 * The initial value of this property is CGPointZero.
 */
@property(nonatomic, assign) CGPoint playerLocationControl;

/**
 *  enableBulletSolver
 */
-(void) enableBulletSolver:(BOOL)enable;

@end
