//
//  MixColliderScene.mm
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "MixColliderScene.h"
#import "CC3PODResourceNode.h"
#import "CC3ActionInterval.h"
#import "CC3MeshNode.h"
#import "CC3Camera.h"
#import "CC3Light.h"
#import <OpenGLES/ES2/gl.h>
#import "CC3Material.h"
#import "CC3PFXResource.h"
#import "CC3Node+Physics.h"
#import "CC3BulletSolver.h"

@implementation MixColliderScene

-(void) dealloc
{
	[super dealloc];
}


/**
 *  addSunlight
 */
- (void)addSunlight
{
    CC3Light *light = [CC3Light nodeWithName:@"Lightsun"];
    light.location = cc3v(0,100,0);
    light.isDirectionalOnly = NO;
    [self addChild:light];
}

/**
 *  addSunlight
 */
- (void)addCamera
{
	CC3Camera* cam = [CC3Camera nodeWithName: @"Camera"];
	cam.location = cc3v( 0.0, 0.0, 6.0 );
	[self addChild: cam];
}

/**
 *  Add Physics Nodes
 */
-(void) addPhysicsNodes
{            
    [self addContentFromPODFile:@"collision.pod" withName:@"collision"];
    
    CC3MeshNode *collisionNode = (CC3MeshNode*)[self getNodeNamed:@"collision"];      
    for (CC3Node *childNode in collisionNode.children) {
        if (!childNode.isCamera && !childNode.isLight) {
            // Apply physics node
            if ([childNode.name hasPrefix:kGroundPrefix]) {
                [childNode addPhysicsPassiveRigidBody:CollisionShapeMesh restitution:0.5];
            }
            else if ([childNode.name hasPrefix:kBoxPrefix]) {
                [childNode addPhysicsActiveRigidBody:CollisionShapeBox mass:1 restitution:1.2];
            }
            else if ([childNode.name hasPrefix:kSpherePrefix]) {
                [childNode addPhysicsActiveRigidBody:CollisionShapeSphere mass:1 restitution:0.8];
            }
            
            // Add node to physics world
            if (childNode.physics != nil) {
                [self.bulletSolver addPhysics:childNode.physics];
            }
        }
    }     
}

/**
 * Constructs the 3D scene prior to the scene being displayed.
 *
 * Adds 3D objects to the scene, loading a 3D 'hello, world' message
 * from a POD file, and creating the camera and light programatically.
 *
 * When adapting this template to your application, remove all of the content
 * of this method, and add your own to construct your 3D model scene.
 *
 * You can also load scene content asynchronously while the scene is being displayed by
 * loading on a background thread. The
 *
 * NOTES:
 *
 * 1) To help you find your scene content once it is loaded, the onOpen method below contains
 *    code to automatically move the camera so that it frames the scene. You can remove that
 *    code once you know where you want to place your camera.
 *
 * 2) The POD file used for the 'hello, world' message model is fairly large, because converting a
 *    font to a mesh results in a LOT of triangles. When adapting this template project for your own
 *    application, REMOVE the POD file 'hello-world.pod' from the Resources folder of your project.
 */
-(void) initializeScene
{
    self.playerDirectionControl = CGPointZero;
    self.playerLocationControl = CGPointZero;
    
    // Support collision
    //[self enablePhysics:YES];
    
    [self enableBulletSolver:YES];
    
	// Create the camera, place it back a bit, and add it to the scene
    [self addCamera];
    
    // Add main camera
    [self addSunlight];
    
	// This is the simplest way to load a POD resource file and add the
	// nodes to the CC3Scene, if no customized resource subclass is needed.
    [self addPhysicsNodes];
    
	// Create OpenGL buffers for the vertex arrays to keep things fast and efficient, and to
	// save memory, release the vertex content in main memory because it is now redundant.
	[self createGLBuffers];
	[self releaseRedundantContent];
	
	// Select an appropriate shader program for each mesh node in this scene now. If this step
	// is omitted, a shader program will be selected for each mesh node the first time that mesh
	// node is drawn. Doing it now adds some additional time up front, but avoids potential pauses
	// as each shader program is loaded as needed the first time it is needed during drawing.
	[self selectShaderPrograms];
    
	// With complex scenes, the drawing of objects that are not within view of the camera will
	// consume GPU resources unnecessarily, and potentially degrading app performance. We can
	// avoid drawing objects that are not within view of the camera by assigning a bounding
	// volume to each mesh node. Once assigned, the bounding volume is automatically checked
	// to see if it intersects the camera's frustum before the mesh node is drawn. If the node's
	// bounding volume intersects the camera frustum, the node will be drawn. If the bounding
	// volume does not intersect the camera's frustum, the node will not be visible to the camera,
	// and the node will not be drawn. Bounding volumes can also be used for collision detection
	// between nodes. You can create bounding volumes automatically for most rigid (non-skinned)
	// objects by using the createBoundingVolumes on a node. This will create bounding volumes
	// for all decendant rigid mesh nodes of that node. Invoking the method on your scene will
	// create bounding volumes for all rigid mesh nodes in the scene. Bounding volumes are not
	// automatically created for skinned meshes that modify vertices using bones. Because the
	// vertices can be moved arbitrarily by the bones, you must create and assign bounding
	// volumes to skinned mesh nodes yourself, by determining the extent of the bounding
	// volume you need, and creating a bounding volume that matches it. Finally, checking
	// bounding volumes involves a small computation cost. For objects that you know will be
	// in front of the camera at all times, you can skip creating a bounding volume for that
	// node, letting it be drawn on each frame.
	[self createBoundingVolumes];
	LogInfo(@"The structure of this scene is: %@", [self structureDescription]);
    
	// Perfom POD animation
}

/**
 * By populating this method, you can add add additional scene content dynamically and
 * asynchronously after the scene is open.
 *
 * This method is invoked from a code block defined in the onOpen method, that is run on a
 * background thread by the CC3GLBackgrounder available through the backgrounder property of
 * the viewSurfaceManager. It adds content dynamically and asynchronously while rendering is
 * running on the main rendering thread.
 *
 * You can add content on the background thread at any time while your scene is running, by
 * defining a code block and running it on the backgrounder of the viewSurfaceManager. The
 * example provided in the onOpen method is a template for how to do this, but it does not
 * need to be invoked only from the onOpen method.
 *
 * Certain assets, notably shader programs, will cause short, but unavoidable, delays in the
 * rendering of the scene, because certain finalization steps from shader compilation occur on
 * the main thread. Shaders and certain other critical assets should be pre-loaded in the
 * initializeScene method prior to the opening of this scene.
 */
-(void) addSceneContentAsynchronously
{
}

/**
 *
 */
-(void) resetActiveCamera
{
}

#pragma mark Updating custom activity

/**
 * This template method is invoked periodically whenever the 3D nodes are to be updated.
 *
 * This method provides your app with an opportunity to perform update activities before
 * any changes are applied to the transformMatrix of the 3D nodes in the scene.
 *
 * For more info, read the notes of this method on CC3Node.
 */
-(void) updateBeforeTransform: (CC3NodeUpdatingVisitor*) visitor
{
    glClearColor(83/255., 83/255., 83/255., 1);
    [self updateCameraFromControls: visitor.deltaTime];
}

/**
 * This template method is invoked periodically whenever the 3D nodes are to be updated.
 *
 * This method provides your app with an opportunity to perform update activities after
 * the transformMatrix of the 3D nodes in the scen have been recalculated.
 *
 * For more info, read the notes of this method on CC3Node.
 */
-(void) updateAfterTransform: (CC3NodeUpdatingVisitor*) visitor
{
    
    static NSInteger limmited = 0;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (limmited > 5) return ;
        limmited++;
        @synchronized(self) {
         //[self.physicsWorld synchTransformation];
            [self.bulletSolver syncTranformation];
            limmited--;
        }
    });     
}

/** Update the location and direction of looking of the 3D camera */
-(void) updateCameraFromControls: (ccTime) dt
{
	CC3Camera* cam = self.activeCamera;
	//CC3Vector center = [self getNodeNamed:@"house"].centerOfGeometry;
    
	// Update the location of the player (the camera)
	if ( _playerLocationControl.x || _playerLocationControl.y ) {
		
		// Get the X-Y delta value of the control and scale it to something suitable
		CGPoint delta = ccpMult(_playerLocationControl, dt * 30.0);
        
		// We want to move the camera forward and backward, and side-to-side,
		// from the camera's (the user's) point of view.
		// Forward and backward will be along the globalForwardDirection of the camera,
		// and side-to-side will be along the globalRightDirection of the camera.
		// These two directions are scaled by Y and X delta components respectively, which
		// in turn are set by the joystick, and combined into a single directional vector.
		// This represents the movement of the camera. The new location is simply the old
		// camera location plus the movement.
		CC3Vector moveVector = CC3VectorAdd(CC3VectorScaleUniform(cam.globalRightDirection, delta.x),
											CC3VectorScaleUniform(cam.globalForwardDirection, delta.y));
		cam.location = CC3VectorAdd(cam.location, moveVector);
	}
    
	// Update the direction the camera is pointing by panning and inclining using rotation.
	if ( _playerDirectionControl.x || _playerDirectionControl.y ) {
		CGPoint delta = ccpMult(_playerDirectionControl, dt * 15.0);		// Factor to set speed of rotation.
		CC3Vector camRot = cam.rotation;
		camRot.y -= delta.x;
		camRot.x += delta.y;
		cam.rotation = camRot;
	}
}


#pragma mark Scene opening and closing

/**
 * Callback template method that is invoked automatically when the CC3Layer that
 * holds this scene is first displayed.
 *
 * This method is a good place to invoke one of CC3Camera moveToShowAllOf:... family
 * of methods, used to cause the camera to automatically focus on and frame a particular
 * node, or the entire scene.
 *
 * For more info, read the notes of this method on CC3Scene.
 */
-(void) onOpen
{
	
	// Add additional scene content dynamically and asynchronously on a background thread
	// after the scene is open and rendering has begun on the rendering thread. We use the
	// GL backgrounder provided by the viewSurfaceManager to accomplish this. Asynchronous
	// loading must be initiated after the scene has been attached to the view. It cannot
	// be started in the initializeScene method. However, you do not need to start it only
	// in this onOpen method. You can use the code here as a template for use whenever your
	// app requires background content loading.
	[self.viewSurfaceManager.backgrounder runBlock: ^{
		[self addSceneContentAsynchronously];
	}];
    
	// Move the camera to frame the scene. The resulting configuration of the camera is output as
	// a [debug] log message, so you know where the camera needs to be in order to view your scene.
    //[self setShouldDrawAllWireframeBoxes:YES];
	[self.activeCamera moveWithDuration: 1.0 toShowAllOf: self withPadding: 0.05f];
}

/**
 * Callback template method that is invoked automatically when the CC3Layer that
 * holds this scene has been removed from display.
 *
 * For more info, read the notes of this method on CC3Scene.
 */
-(void) onClose
{
}


#pragma mark Drawing

/**
 * Template method that draws the content of the scene.
 *
 * This method is invoked automatically by the drawScene method, once the 3D environment has
 * been established. Once this method is complete, the 2D rendering environment will be
 * re-established automatically, and any 2D billboard overlays will be rendered. This method
 * does not need to take care of any of this set-up and tear-down.
 *
 * This implementation turns on the lighting contained within the scene, and performs a single
 * rendering pass of the nodes in the scene by invoking the visit: method on the specified
 * visitor, with this scene as the argument.
 *
 * You can override this method to customize the scene rendering flow, such as performing
 * multiple rendering passes on different surfaces, or adding post-processing effects, using
 * the template methods mentioned above.
 *
 * Rendering output is directed to the render surface held in the renderSurface property of
 * the visitor. By default, that is set to the render surface held in the viewSurface property
 * of this scene. If you override this method, you can set the renderSurface property of the
 * visitor to another surface, and then invoke this superclass implementation, to render this
 * scene to a texture for later processing.
 *
 * When overriding the drawSceneContentWithVisitor: method with your own specialized rendering,
 * steps, be careful to avoid recursive loops when rendering to textures and environment maps.
 * For example, you might typically override drawSceneContentWithVisitor: to include steps to
 * render environment maps for reflections, etc. In that case, you should also override the
 * drawSceneContentForEnvironmentMapWithVisitor: to render the scene without those additional
 * steps, to avoid the inadvertenly invoking an infinite recursive rendering of a scene to a
 * texture while the scene is already being rendered to that texture.
 *
 * To maintain performance, by default, the depth buffer of the surface is not specifically
 * cleared when 3D drawing begins. If this scene is drawing to a surface that already has
 * depth information rendered, you can override this method and clear the depth buffer before
 * continuing with 3D drawing, by invoking clearDepthContent on the renderSurface of the visitor,
 * and then invoking this superclass implementation, or continuing with your own drawing logic.
 *
 * Examples of when the depth buffer should be cleared are when this scene is being drawn
 * on top of other 3D content (as in a sub-window), or when any 2D content that is rendered
 * behind the scene makes use of depth drawing. See also the closeDepthTestWithVisitor:
 * method for more info about managing the depth buffer.
 */
-(void) drawSceneContentWithVisitor: (CC3NodeDrawingVisitor*) visitor
{
	[self illuminateWithVisitor: visitor];		// Light up your world!
	[visitor visit: self.backdrop];				// Draw the backdrop if it exists
	[visitor visit: self];						// Draw the scene components
	[self drawShadows];							// Shadows are drawn with a different visitor
}


#pragma mark Handling touch events

/**
 * This method is invoked from the CC3Layer whenever a touch event occurs, if that layer
 * has indicated that it is interested in receiving touch events, and is handling them.
 *
 * Override this method to handle touch events, or remove this method to make use of
 * the superclass behaviour of selecting 3D nodes on each touch-down event.
 *
 * This method is not invoked when gestures are used for user interaction. Your custom
 * CC3Layer processes gestures and invokes higher-level application-defined behaviour
 * on this customized CC3Scene subclass.
 *
 * For more info, read the notes of this method on CC3Scene.
 */
-(void) touchEvent: (uint) touchType at: (CGPoint) touchPoint
{
}

/**
 * This callback template method is invoked automatically when a node has been picked
 * by the invocation of the pickNodeFromTapAt: or pickNodeFromTouchEvent:at: methods,
 * as a result of a touch event or tap gesture.
 *
 * Override this method to perform activities on 3D nodes that have been picked by the user.
 *
 * For more info, read the notes of this method on CC3Scene.
 */
-(void) nodeSelected: (CC3Node*) aNode byTouchEvent: (uint) touchType at: (CGPoint) touchPoint
{
}

#pragma mark- Update settings

/**
 *   Update settings
 */
-(void) updateSetting:(CC3Layer *)layer
{
    [super updateSetting:layer];
    [self resetActiveCamera];
}

@end
