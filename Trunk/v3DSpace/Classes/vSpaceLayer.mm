/**
 *  v3DSpaceLayer.mm
 *  v3DSpace
 *
 *  Created by Sinbad Flyce on 11/1/13.
 *  Copyright VMODEV JSC 2013. All rights reserved.
 */

#import "vSpaceLayer.h"
#import "HouseScene.h"
#import "SettingViewController.h"

#define kJoystickThumbFileName			@"joytick_thumb.png"
#define kJoystickSideLength				120.0
#define kJoystickPadding				4.0
#define kButtonGrid						40.0

#define kSettingButtonFileName          @"btn_setting.png"
#define kSettingTouchedButtonFileName   @"btn_setting_touched.png"
#define kSettingButtonSize              84.0

@interface CC3Layer (TemplateMethods)

-(BOOL) handleTouch: (UITouch*) touch ofType: (uint) touchType;

@end

@implementation vSpaceLayer

-(void) dealloc
{
    [directionJoystick release];
    [locationJoystick release];
    [super dealloc];
}

/*!
 * Positions the right-side location joystick at the right of the layer.
 * This is called at initialization, and anytime the content size of the layer changes
 * to keep the joystick in the correct location within the new layer dimensions.
 */
-(void) positionLocationJoystick
{
	locationJoystick.position = ccp(self.contentSize.width - kJoystickSideLength - kJoystickPadding, kJoystickPadding);
}

/**
 * Creates a button (actually a single-item menu) in the top right of the layer that will
 * allow the user to turn the Setting
 */
-(void) positionLocationSetting
{
    GLfloat h = self.contentSize.height;
    settingMI.position = ccp(kJoystickPadding + kSettingButtonSize/2, h - kSettingButtonSize/2 - kJoystickPadding);
}

/*!
 * Creates a button (actually a single-item menu) in the bottom center of the layer
 * that will allow the user to toggle shadows on and off for a selected node.
 */
-(void) addJoysticks
{
	CCSprite* jsThumb;
    
	// Change thumb scale if you like smaller or larger controls.
	// Initially, just compensate for Retina display.
	GLfloat thumbScale = CC_CONTENT_SCALE_FACTOR();
    
	// The joystick that controls the player's (camera's) direction
	jsThumb = [CCSprite spriteWithFile: kJoystickThumbFileName];
	jsThumb.scale = thumbScale;
	
	directionJoystick = [Joystick joystickWithThumb: jsThumb
											andSize: CGSizeMake(kJoystickSideLength, kJoystickSideLength)];
	
	// If you want to see the size of the Joystick backdrop, comment out the line above
	// and uncomment the three lines below. This just adds a simple bland colored backdrop
	// to demonstrate that the thumb and backdrop can be any CCNode, but normally you
	// would use a nice graphical CCSprite for the Joystick backdrop.
    // CCLayer* jsBackdrop = [CCLayerColor layerWithColor: ccc4(255, 255, 255, 63)
    // 											 width: kJoystickSideLength height: kJoystickSideLength];
    //	jsBackdrop.isRelativeAnchorPoint = YES;
    //	directionJoystick = [Joystick joystickWithThumb: jsThumb andBackdrop: jsBackdrop];
	
	directionJoystick.position = ccp(kJoystickPadding, kJoystickPadding);
	[self addChild: directionJoystick];
	
	// The joystick that controls the player's (camera's) location
	jsThumb = [CCSprite spriteWithFile: kJoystickThumbFileName];
	jsThumb.scale = thumbScale;
	
	locationJoystick = [Joystick joystickWithThumb: jsThumb
										   andSize: CGSizeMake(kJoystickSideLength, kJoystickSideLength)];
	[self positionLocationJoystick];
	[self addChild: locationJoystick];
}

/**
 * Creates a button (actually a single-item menu) in the top right of the layer that will
 * allow the user to turn the Setting
 */
-(void) addSettingButton
{
	
	// Set up the menu item and position it in the bottom center of the layer
	settingMI = [AdornableMenuItemImage itemWithNormalImage: kSettingButtonFileName
											   selectedImage: kSettingTouchedButtonFileName
													  target: self
													selector: @selector(onSetting:)];
	[self positionLocationSetting];
	CCMenu* viewMenu = [CCMenu menuWithItems: settingMI, nil];
	viewMenu.position = CGPointZero;    
	[self addChild: viewMenu];
}

/*!
 * Override to set up your 2D controls and other initial state, and to initialize update processing.
 *
 * For more info, read the notes of this method on CC3Layer.
 */
-(void) initializeControls
{
    [self setTouchEnabled:NO];
    [self setMouseEnabled:YES];
    [self addJoysticks];
    [self addSettingButton];
	[self scheduleUpdate];
}

/**
 * Returns the contained CC3Scene, cast into the appropriate type.
 * This is a convenience method to perform automatic casting.
 */
-(vSpaceScene*) scense
{
    return (vSpaceScene*) self.cc3Scene;
}

#pragma mark Updating layer

/*!
 * Override to perform set-up activity prior to the scene being opened
 * on the view, such as adding gesture recognizers.
 *
 * For more info, read the notes of this method on CC3Layer.
 */
-(void) onOpenCC3Layer
{
}

/*!
 * Override to perform tear-down activity prior to the scene disappearing.
 *
 * For more info, read the notes of this method on CC3Layer.
 */
-(void) onCloseCC3Layer
{
}

/**
 * The ccTouchMoved:withEvent: method is optional for the <CCTouchDelegateProtocol>.
 * The event dispatcher will not dispatch events for which there is no method
 * implementation. Since the touch-move events are both voluminous and seldom used,
 * the implementation of ccTouchMoved:withEvent: has been left out of the default
 * CC3Layer implementation. To receive and handle touch-move events for object
 * picking, it must be implemented here.
 *
 * This method will not be invoked if gestures have been enabled.
 */
-(void) ccTouchMoved: (UITouch *)touch withEvent: (UIEvent *)event
{
	[self handleTouch: touch ofType: kCCTouchMoved];
}

/*!
 * Updates the player (camera) direction and location from the joystick controls
 * and then updates the 3D scene.
 */
-(void) update: (ccTime)dt
{
	// Update the player direction and position in the scene from the joystick velocities
    [self scense].playerLocationControl = locationJoystick.velocity;
    [self scense].playerDirectionControl = directionJoystick.velocity;
	[super update: dt];
}

/**
 * Called automatically when the contentSize has changed.
 * Move the location joystick to keep it in the bottom right corner of this layer
 * and the switch view button to keep it centered between the two joysticks.
 */
-(void) didUpdateContentSizeFrom: (CGSize) oldSize
{
	[super didUpdateContentSizeFrom: oldSize];
	[self positionLocationJoystick];
    [self positionLocationSetting];
}

#pragma mark- Actions

/**
 *  onSetting
 */
-(void) onSetting: (id)svMI
{
    SettingViewController *settingVC = [[[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil] autorelease];
    [settingVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [settingVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [settingVC setDelegate:self];
    [self.controller presentModalViewController:settingVC animated:YES];
}

#pragma mark- SettingViewDelegate

-(void) settingController:(UIViewController *)controller shouldChangeAnimation:(BOOL)shouldAnimation
{
}

-(void) settingController:(UIViewController *)controller shouldChangeDaynight:(BOOL)shouldDaynight
{
}

-(void) settingController:(UIViewController *)controller shouldResetCamera:(BOOL)shouldReset
{
    if (shouldReset) {
        [[self scense] updateSetting:self];
    }
}

@end
