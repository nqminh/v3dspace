//
//  House.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/10/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "CC3MeshNode.h"

#define VHOUSE_SPHERE1  @"pSphere1"
#define VHOUSE_SPHERE2  @"pSphere2"
#define VHOUSE_SPHERE3  @"pSphere3"
#define VHOUSE_TREE     @"Tree"

@interface House : CC3MeshNode

-(void) recoverProblemNodesInSence:(CC3Scene*)sence;

@end
