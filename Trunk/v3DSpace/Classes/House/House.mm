//
//  House.mm
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/10/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "House.h"
#import "CC3Node.h"
#import "CC3PODResourceNode.h"
#import "CC3ActionInterval.h"
#import "CC3Camera.h"
#import "CC3Light.h"
#import <OpenGLES/ES2/gl.h>
#import "CC3Material.h"
#import "CC3PFXResource.h"

@interface House ()
@end

@implementation House

/**
 *  Init from POD file
 */
-(id) init
{
    self = [super init];
    
    if (self) {
        [self setName:@"house"];
        [self addContentFromPODFile:@"house.pod"];
        [self setShouldSmoothLines:YES];
    }
    
    return self;
}

/**
 *  There are a number of transform nodes incorrectly the conversion from DAE -> POD.
 *  So we need this function.
 */
-(void) recoverProblemNodesInSence:(CC3Scene*)sence
{
    for (CC3Light *light in [sence lights ]) {
        light.forwardDirection = cc3v(light.forwardDirection.x,-light.forwardDirection.y,light.forwardDirection .z);
    }
    
    CC3Node *pSphere2 = [self getNodeNamed:VHOUSE_SPHERE2];
    CC3Node *pSphere3 = [self getNodeNamed:VHOUSE_SPHERE3];
    CC3Node *pTree = [self getNodeNamed:VHOUSE_TREE];
    pSphere2.shouldUseLighting = FALSE;
    pSphere3.shouldUseLighting = FALSE;
    pTree.visible = NO;
}

@end
