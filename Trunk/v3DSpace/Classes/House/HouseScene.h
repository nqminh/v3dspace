/*
 *  HouseScene.h
 *  v3DSpace
 *
 *  Created by Sinbad Flyce on 11/1/13.
 *  Copyright VMODEV JSC 2013. All rights reserved.
 */

                
#import "CC3Scene.h"
#import "vSpaceScene.h"

@class CC3PhysicsWorld;
@class House;



/*! A sample application-specific CC3Scene subclass.
 *  Will be implemented soon.
 */
@interface HouseScene : vSpaceScene

@property(nonatomic, readonly) House* house;

@end
