//
//  SettingViewController.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/11/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Delegate protocol 
 */
@protocol SettingViewDelegate <NSObject>
- (void)settingController:(UIViewController*)controller shouldResetCamera:(BOOL)shouldReset;
- (void)settingController:(UIViewController*)controller shouldChangeAnimation:(BOOL)shouldAnimation;
- (void)settingController:(UIViewController*)controller shouldChangeDaynight:(BOOL)shouldDaynight;
@end

@interface SettingViewController : UIViewController

// Delegate
@property (nonatomic, assign) id<SettingViewDelegate> delegate;

// Actions
- (IBAction)onSwitchChanged:(UISwitch*)sender;
- (IBAction)onDone:(id)sender;

@end
