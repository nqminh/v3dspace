//
//  SettingViewController.m
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/11/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import "SettingViewController.h"

#define kDaynightSwitch     1000
#define kAnimationSwitch    1001
#define kResetCameraSwitch  1002

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return  (toInterfaceOrientation == UIDeviceOrientationLandscapeRight || toInterfaceOrientation == UIDeviceOrientationLandscapeLeft);
}


- (IBAction)onSwitchChanged:(UISwitch*)sender
{
    if (self.delegate) {
        switch (sender.tag) {
            case kDaynightSwitch:
                if ([self.delegate respondsToSelector:@selector(settingController:shouldChangeDaynight:)])
                    [self.delegate settingController:self shouldChangeDaynight:sender.on];
                break;
            case kAnimationSwitch:
                if ([self.delegate respondsToSelector:@selector(settingController:shouldChangeAnimation:)])
                    [self.delegate settingController:self shouldChangeAnimation:sender.on];
                break;
            case kResetCameraSwitch:
                if ([self.delegate respondsToSelector:@selector(settingController:shouldResetCamera:)])
                    [self.delegate settingController:self shouldResetCamera:sender.on];
                break;                
            default:
                break;
        }
    }
}

- (IBAction)onDone:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
