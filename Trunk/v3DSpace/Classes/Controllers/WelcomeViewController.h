//
//  WelcomeViewController.h
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/13/13.
//  Copyright (c) 2013 VMODEV JSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WelcomeViewController;

/**
 *  WelcomeViewDelegate protocol
 */
@protocol WelcomeViewDelegate <NSObject>

@required

-(void)welcomeView:(WelcomeViewController*)controller didSelectScene:(NSInteger)indexScene;

@end

/**
 *  WelcomeViewController class
 */
@interface WelcomeViewController : UIViewController

// Delegate
@property (nonatomic,assign) id<WelcomeViewDelegate> delegate;

// Action
- (IBAction)onSelectScence:(UIButton *)sender;

@end
