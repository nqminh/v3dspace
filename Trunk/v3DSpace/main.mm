//
//  main.mm
//  v3DSpace
//
//  Created by Sinbad Flyce on 11/1/13.
//  Copyright VMODEV JSC 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    sleep(3);
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"v3DSpaceAppDelegate");
    [pool release];
    return retVal;
}
